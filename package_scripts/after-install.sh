getent passwd signaling >/dev/null 2>&1 || adduser \
  --system \
  --shell /usr/sbin/nologin \
  --gecos 'Standalone signaling server for Nextcloud Talk' \
  --group \
  --disabled-password \
  --no-create-home \
  signaling
