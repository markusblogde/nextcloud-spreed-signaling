# [Standalone signaling server for Nextcloud Talk](https://github.com/strukturag/nextcloud-spreed-signaling) Packages

## APT

Requirements:

* systemd (Ubuntu >= 16.04)
* all requirements listed [upstream](https://github.com/strukturag/nextcloud-spreed-signaling)
  * Janus (backported packages [here](https://gitlab.com/packaging/janus/))
  * Nats ([Installation](https://docs.nats.io/nats-server/installation), [Configuration](https://docs.nats.io/nats-server/configuration), [APT packages](https://gitlab.com/packaging/nats-server))
  * ...

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-nextcloud-spreed-signaling.asc https://packaging.gitlab.io/nextcloud-spreed-signaling/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/nextcloud-spreed-signaling signaling main" | sudo tee /etc/apt/sources.list.d/morph027-nextcloud-spreed-signaling.list
```

### Install packages

#### Signaling server

```bash
sudo apt-get update
sudo apt-get install -y nextcloud-spreed-signaling
```

#### Signaling proxy server

```bash
sudo apt-get update
sudo apt-get install -y nextcloud-spreed-signaling-proxy
```

### Configuration

#### Signaling server

```bash
cp /usr/share/signaling/server.conf /etc/signaling/server.conf
```

#### Signaling proxy server

```bash
cp /usr/share/signaling/proxy.conf /etc/signaling/proxy.conf
```

## Docker

### Run

Image builds can be found [here](https://gitlab.com/packaging/nextcloud-spreed-signaling/container_registry).

Examples:

```bash
# bind mount override with default listener
docker run -d --name signaling -v /my-local-folder/server.conf:/config/server.conf:ro -p 8081:8081 registry.gitlab.com/packaging/nextcloud-spreed-signaling:latest

# bind mount with proper setting and config with listener on 18081
docker run -d --name signaling -v /my-local-folder/server.conf:/my-config/server.conf:ro -e CONFIG=/my-config/server.conf -e 18081:18081 registry.gitlab.com/packaging/nextcloud-spreed-signaling:latest
```

**NOTICE**: When running in Docker and you're also running Janus, CoTURN, Nats, ... in docker, you need to make sure the containers can reach each other, e.g. creating a dedicated network: `docker network create hbp`.
You can then attach containers using `--network hbp` (or much better: using docker-compose) and use the container names in the config files.
