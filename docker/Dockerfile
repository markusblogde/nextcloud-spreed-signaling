FROM	golang:1.14 AS builder

RUN	apt-get update \
	&& apt-get install -y python3

RUN	(go get github.com/strukturag/nextcloud-spreed-signaling || true) \
	&& cd /go/src/github.com/strukturag/nextcloud-spreed-signaling \
	&& make build

FROM	alpine:3.12

ENV	CONFIG=/config/server.conf

RUN	adduser -D spreedbackend && \
	apk add --no-cache --no-cache ca-certificates libc6-compat libstdc++

USER	spreedbackend

COPY	--from=builder /go/src/github.com/strukturag/nextcloud-spreed-signaling/bin/signaling /usr/local/bin/signaling

COPY	--from=builder /go/src/github.com/strukturag/nextcloud-spreed-signaling/server.conf.in /config/server.conf

COPY	--from=builder /go/src/github.com/strukturag/nextcloud-spreed-signaling/bin/proxy /usr/local/bin/proxy

COPY	--from=builder /go/src/github.com/strukturag/nextcloud-spreed-signaling/proxy.conf.in /config/proxy.conf

CMD	["/bin/sh", "-c", "exec /usr/local/bin/signaling --config=$CONFIG"]
